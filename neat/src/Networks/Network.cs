﻿using System;
using System.Collections.Generic;
using neat.src.Layers;

namespace neat.src.Networks
{
    [Serializable]
    public class Network
    {
        /// <summary>
        /// the score this network has reached
        /// </summary>
        private double Score = 0;
        /// <summary>
        /// the count of input neurons
        /// </summary>
        private List<int> inputs = new List<int>();
        /// <summary>
        /// the count of output neurons
        /// </summary>
        private List<int> outputs = new List<int>();
        /// <summary>
        /// the hidden layers of this network
        /// </summary>
        private List<Layer> HiddenLayer = new List<Layer>();


        /// <summary>
        /// the neat object this network is part of
        /// </summary>
        public Neat Neat;
        /// <summary>
        /// the random object of the neat object
        /// </summary>
        public Random Random { get => Neat.Random; }
        /// <summary>
        /// the list of connections between neurons
        /// </summary>
        public List<Genome> Genomes = new List<Genome>();


        public Network()
        {

        }


        /// <summary>
        /// run this network with the specified inputs
        /// </summary>
        /// <param name="Inputs">the inputs to calculate</param>
        public List<double> Run(List<double> Inputs)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// run this network with the specified inputs
        /// </summary>
        /// <param name="Inputs">the inputs to calculate</param>
        public List<double> Run(params double[] Inputs)
        {
            throw new NotImplementedException();
        }
    }
}
