﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;

namespace neat.helper.src
{
    public class AcceleratedThreadingPool
    {
        #region properties

        /// <summary>
        /// number of workers
        /// </summary>
        private int workers { get; set; }
        /// <summary>
        /// the number of calculations per iteration per thread
        /// </summary>
        private int calculations { get; set; }
        /// <summary>
        /// the sum of all calculations
        /// </summary>
        private int sumcalculations { get; set; }
        /// <summary>
        /// the number iterations that run
        /// </summary>
        private int iterations { get; set; }
        /// <summary>
        /// the average calculations per iteration
        /// </summary>
        private float averagecalculations { get; set; }
        /// <summary>
        /// actual threads
        /// </summary>
        private List<Thread> threads { get; set; }
        /// <summary>
        /// the list of actions to be processed
        /// </summary>
        private Queue<Action> actions { get; set; }
        /// <summary>
        /// the main operator thread
        /// </summary>
        private Thread administrator { get; set; }
        /// <summary>
        /// the action to be processed after finishing all other actions
        /// </summary>
        private Action finished { get; set; }
        /// <summary>
        /// whether or not the operator may run
        /// </summary>
        private bool run { get; set; }
        /// <summary>
        /// whether or not optimization may happen
        /// </summary>
        private bool mayoptimize { get; set; }
        /// <summary>
        /// last time measured
        /// </summary>
        private int lastMeasureTime { get; set; }
        /// <summary>
        /// current time measured
        /// </summary>
        private int currentMeasureTime { get; set; }

        #endregion

        #region constructors

        /// <summary>
        /// initializes new instance of an accelerated threading pool
        /// </summary>
        /// <param name="finished">the action to be processed after finishing all other actions</param>
        /// <param name="workers">number of workers</param>
        public AcceleratedThreadingPool(Action finished = null, int workers = 10, bool mayoptimize = true)
        {
            actions = new Queue<Action>();
            this.mayoptimize = mayoptimize;
            this.workers = workers;
            threads = new List<Thread>();
            for (int n = 0; n < workers; n++)
                threads.Add(new Thread(() => { }));
            this.finished = finished;
            calculations = 1;
            run = false;
            lastMeasureTime = 0;
            currentMeasureTime = 0;
            iterations = 0;
            averagecalculations = 0;
        }

        #endregion

        #region public methods

        /// <summary>
        /// starts the execution of actions
        /// </summary>
        public void Start()
        {
            administrator = new Thread(() => operate());
            run = true;
            administrator.Start();
        }

        /// <summary>
        /// stops the execution of actions
        /// </summary>
        public void Stop()
        {
            run = false;
            administrator.Abort();
        }

        /// <summary>
        /// clear the actions list
        /// </summary>
        public void Clear()
        {
            actions.Clear();
        }

        /// <summary>
        /// Add the specified action.
        /// </summary>
        /// <param name="action">action to be processed by the threading pool</param>
        public void Add(Action action)
        {
            actions.Enqueue(action);
        }

        public float GetAverageCalculations()
        {
            return averagecalculations;
        }

        #endregion

        #region private methods

        private void operate()
        {
            while (run)
            {
                // only run if there are actions left
                if (actions.Count <= 0 && allThreadsFinished())
                {
                    finished?.Invoke();
                    run = false;
                    Stop();
                    return;
                }
                var currentActions = new List<Action>();
                var thread = -1;


                // find free thread and save him
                for (int n = 0; n < workers; n++)
                    if (!threads[n].IsAlive)
                    {
                        thread = n;
                        break;
                    }

                // get all threads to be processed next
                if (thread > (-1))
                {
                    for (int n = 0; n < calculations; n++)
                        if (actions.Count > 0)
                            currentActions.Add(actions.Dequeue());

                    threads[thread] = new Thread(() => execute(currentActions));
                    threads[thread].Start();
                }

                if (mayoptimize)
                    optimize();
            }
        }

        private bool allThreadsFinished()
        {
            foreach (var thread in threads)
                if (thread.IsAlive)
                    return false;
            return true;
        }

        private void execute(List<Action> actions)
        {
            var t = measure();
            foreach (var action in actions)
                action?.Invoke();
            endmeasure(t);
        }

        private Stopwatch measure()
        {
            var t = new Stopwatch();
            t.Start();
            return t;
        }

        private void endmeasure(Stopwatch t)
        {
            t.Stop();
            currentMeasureTime = (int)t.ElapsedMilliseconds;
            t = null;
        }

        private void optimize()
        {
            setaverage();

            var lastTime = (lastMeasureTime / (float)iterations) * (iterations + 1);

            if (lastTime < currentMeasureTime)
                calculations++;
            else if (lastTime > currentMeasureTime)
                calculations--;

            if (calculations <= 0)
                calculations = 1;

            lastMeasureTime = currentMeasureTime;
        }

        private void setaverage()
        {
            iterations++;
            sumcalculations += calculations;
            averagecalculations = (sumcalculations / (float)iterations);
        }

        #endregion

    }
}
