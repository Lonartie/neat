﻿using System;
using System.Collections.Generic;

namespace neat.src.ActivationFunctions
{
    /// <summary>
    /// interface for activation function
    /// </summary>
    public interface IActivationFunction
    {
        /// <summary>
        /// returns the calculated value of that activation function
        /// </summary>
        /// <param name="value">value</param>
        double function(double value);
    }

    public static class AllActivationFunctions
    {
        public static Dictionary<string, IActivationFunction> functions = new Dictionary<string, IActivationFunction>();

        static AllActivationFunctions()
        {
            functions.Add("tanh", new tanh());
            functions.Add("sin", new sin());
            functions.Add("cos", new cos());
            functions.Add("abs", new abs());
            functions.Add("clamped", new clamped());
            functions.Add("cubed", new cubed());
            functions.Add("squared", new squared());
            functions.Add("sigmoid", new sigmoid());
        }
    }
}
