﻿using System;
using System.Threading;
using neat.src;
using neat.src.Networks;

namespace neat.debug.src
{
    public class XOR
    {
        static bool finished = false;
        static Neat neat = null;

        public static void Start()
        {
            var options = new NeatOptions
            {
                Inputs = 2,
                Outputs = 2,
                Debug = true,
                AcceleratedTraining = false,
                OnTrainingFinished = Finished,
            };

            neat = Neat.Create(options);
            neat.TrainToError(.001d, (network) =>
            {
                var result = 0d;

                result += Math.Abs(0 - network.Run(0, 0)[0]);
                result += Math.Abs(1 - network.Run(0, 1)[0]);
                result += Math.Abs(1 - network.Run(1, 0)[0]);
                result += Math.Abs(0 - network.Run(0, 0)[0]);

                return result;
            });

            while (!finished)
                Thread.Sleep(1);
        }

        static void Finished()
        {
            finished = true;

            var output00 = neat.Run(0, 0)[0];
            var output01 = neat.Run(0, 1)[0];
            var output10 = neat.Run(1, 0)[0];
            var output11 = neat.Run(1, 1)[0];

            Console.WriteLine($"inputs: [0,0]\r\toutput: [{output00}]\r\t expected: [0] error: [{Math.Abs(0 - output00) * 100}%]");
            Console.WriteLine($"inputs: [0,1]\r\toutput: [{output01}]\r\t expected: [1] error: [{Math.Abs(1 - output01) * 100}%]");
            Console.WriteLine($"inputs: [1,0]\r\toutput: [{output10}]\r\t expected: [1] error: [{Math.Abs(1 - output10) * 100}%]");
            Console.WriteLine($"inputs: [1,1]\r\toutput: [{output11}]\r\t expected: [0] error: [{Math.Abs(0 - output11) * 100}%]");
        }
    }
}
