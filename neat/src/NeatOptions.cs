﻿using System;
using System.Xml.Serialization;

namespace neat.src
{
    /// <summary>
    /// options which defines the behaviour of a neat object
    /// </summary>
    public class NeatOptions
    {
        /// <summary>
        /// count of inputs layers
        /// </summary>
        public int Inputs = 1;
        /// <summary>
        /// count of outputs layers
        /// </summary>
        public int Outputs = 1;
        /// <summary>
        /// count of networks
        /// </summary>
        public int Networks = 100;
        /// <summary>
        /// whether or not the training should use an experimental method for performance gain
        /// </summary>
        public bool AcceleratedTraining = false;
        /// <summary>
        /// whether or not to throw debug messages 
        /// </summary>
        public bool Debug = false;
        /// <summary>
        /// method which will be invoked after a training session has finished
        /// </summary>
        [XmlIgnore] public Neat.OnTrainingFinished OnTrainingFinished { get; set; }
    }
}
