﻿using System;
namespace neat.src.Networks
{
    public class Genome
    {
        /// <summary>
        /// the wheight of this genome which is used to multiply with its inputs
        /// </summary>
        public double wheight = 0d;
        /// <summary>
        /// the input neuron
        /// </summary>
        public Neuron Input;
        /// <summary>
        /// the output neuron
        /// </summary>
        public Neuron Output;

        /// <summary>
        /// gets this genomes output value based on its input values times its wheight
        /// </summary>
        public double GetValue() =>
            Input.Calculate() * wheight;
    }
}
