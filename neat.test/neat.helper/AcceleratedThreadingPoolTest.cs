﻿using System;
using System.Threading;
using neat.helper.src;
using NUnit.Framework;
using System.Diagnostics;

[TestFixture]
public class AcceleratedThreadingPoolTest : ITestFixture
{

    [Test]
    public void AllActionsDoneTest()
    {
        var x = 0;
        var finished = false;
        var pool = new AcceleratedThreadingPool(() => finished = true, 10, false);
        for (int n = 0; n < 100; n++)
            pool.Add(() => x++);
        pool.Start();
        while (!finished)
            Thread.Sleep(1);
        Result(x, 100);
    }

    [Test]
    public void OptimizedThreadingPoolIsFasterTest()
    {
        throw new NotImplementedException();
    }
    #region data code
    #endregion


}
