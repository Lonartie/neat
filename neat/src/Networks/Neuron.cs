﻿using System;
using System.Collections.Generic;
using neat.src.ActivationFunctions;
using neat.src.Layers;
using neat;

namespace neat.src.Networks
{
    public class Neuron
    {
        public IActivationFunction ActivationFunction;
        public Layer Layer;
        public Network Network;
        public List<Genome> Inputs = new List<Genome>();
        public List<Genome> Outputs = new List<Genome>();

        public double Calculate(double Value = default)
        {
            if (Inputs.Count == 0)
                return Value;

            var sum = 0d;
            foreach (var genome in Inputs)
                sum += genome.GetValue();
            return ActivationFunction.function(sum);
        }

        public void Connect(Neuron otherNeuron, Network network)
        {
            var connection = new Genome();
            connection.wheight = helper.src.Random.GetRandomWheight(-2d, 2d, network.Random);
            connection.Input = this;
            connection.Output = otherNeuron;
            Outputs.Add(connection);
            otherNeuron.Inputs.Add(connection);
            network.Genomes.Add(connection);
        }
    }
}
