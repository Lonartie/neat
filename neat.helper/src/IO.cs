﻿using System;
using System.Xml.Serialization;
using System.Threading.Tasks;
using System.IO;

namespace neat.helper.src
{
    public static class IO
    {
        /// <summary>
        /// write data to the specified path
        /// </summary>
        /// <param name="Path">path where to save the data</param>
        /// <param name="Object">the object to store</param>
        /// <typeparam name="T">the type of object</typeparam>
        public static void Write<T>(string Path, T Object)
        {
            var ser = new XmlSerializer(typeof(T));
            var wri = new StreamWriter(Path);
            ser.Serialize(wri, Object);
            wri.Flush();
            wri.Close();
            wri = null;
            ser = null;
        }

        /// <summary>
        /// read data from the specified path
        /// </summary>
        /// <param name="Path">path where to load the object from</param>
        /// <typeparam name="T">the type which to load</typeparam>
        public static T Read<T>(string Path)
        {
            var ser = new XmlSerializer(typeof(T));
            var rea = new StreamReader(Path);
            var obj = ser.Deserialize(rea);
            rea.Close();
            rea = null;
            ser = null;
            return (T)obj;
        }
    }
}
