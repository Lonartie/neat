﻿using System;
using neat;
using neat.src;
using neat.helper.src;
using System.Threading.Tasks;
using neat.debug.src;
using neat.src.Networks;
using neat.src.ActivationFunctions;

namespace neat.debug
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            //XOR.Start();
            foreach (var item in AllActivationFunctions.functions)
                Console.WriteLine(item.Key);
            Console.ReadKey();
        }
    }
}
