﻿using System;
using System.Collections.Generic;
using neat.helper.src;
using neat.src.Networks;

namespace neat.src
{
    [Serializable]
    public class Neat
    {
        /// <summary>
        /// delegate template for method which will be invoked after a training session has finished
        /// </summary>
        public delegate void OnTrainingFinished(); 


        /// <summary>
        /// settings which will define the behaviour of the neat object
        /// </summary>
        public NeatOptions settings = new NeatOptions();
        /// <summary>
        /// the neuro network
        /// </summary>
        public List<Network> Networks = new List<Network>();
        /// <summary>
        /// the training result data
        /// </summary>
        public ResultData TrainingResult = new ResultData();
        /// <summary>
        /// the random object which is used to handel all randomness in this algorithm
        /// </summary>
        public System.Random Random = new System.Random(DateTime.Now.GetHashCode());


        /// <summary>
        /// creates a new neat object
        /// </summary>
        /// <param name="options">options which defines the neat object</param>
        public static Neat Create(NeatOptions options) =>
            new Neat { settings = options };


        // object 
        /// <summary>
        /// save the neat object
        /// </summary>
        /// <param name="Path">path to save to</param>
        public void Write(string Path) =>
            IO.Write(Path, this);

        // static 
        /// <summary>
        /// load the neat object
        /// </summary>
        /// <returns>The read.</returns>
        /// <param name="Path">path to load from</param>
        public static Neat Read(string Path) =>
            IO.Read<Neat>(Path);


        /// <summary>
        /// train the networks
        /// </summary>
        /// <param name="scoreEvaluation">score evaluation function</param>
        public double Train(Func<Network, double> errorFunction)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// trains the network until that specific error is reached
        /// </summary>
        /// <param name="maxError">the max error to reach</param>
        /// <param name="scoreEvaluation">function which calculates the error for each network</param>
        public void TrainToError(double maxError, Func<Network, double> errorFunction)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// run the best network with the specified inputs
        /// </summary>
        /// <param name="inputs">inputs for network to run with</param>
        public List<double> Run(List<double> inputs)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// run the best network with the specified inputs
        /// </summary>
        /// <param name="inputs">inputs for network to run with</param>
        public List<double> Run(params double[] inputs)
        {
            throw new NotImplementedException();
        }
    }
}
