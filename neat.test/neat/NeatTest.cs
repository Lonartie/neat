﻿using System;
using NUnit.Framework;
using neat.src;
using System.IO;

[TestFixture]
public class NeatTest : ITestFixture
{
    string folder = "/Users/leongierschner/Documents/Test";

    [Test]
    public void SaveNeatTest()
    {
        var neat = Neat.Create(new NeatOptions { OnTrainingFinished = () => { }, AcceleratedTraining = true, Inputs = 1, Outputs = 2 });
        neat.Write(folder + "/model1.neat");
        Result(File.Exists(folder + "/model1.neat"), true);
        File.Delete(folder + "/model1.neat");
    }

    [Test]
    public void LoadNeatTest()
    {
        var neat = Neat.Create(new NeatOptions { AcceleratedTraining = true, Inputs = 1, Outputs = 2 });
        neat.Write(folder + "/model1.neat");
        var neat2 = Neat.Read(folder + "/model1.neat");
        Result(
            (neat.settings.Inputs, neat2.settings.Inputs),
            (neat.settings.Outputs, neat2.settings.Outputs),
            (neat.settings.AcceleratedTraining, neat2.settings.AcceleratedTraining)
        );
    }
}
