﻿using System;
namespace neat.helper.src
{
    public class Random
    {
        /// <summary>
        /// returns a random double between min and max
        /// </summary>
        public static double GetRandomWheight(double min, double max, System.Random random) =>
            random.NextDouble() * Math.Abs(min - max) + min;
    }
}
