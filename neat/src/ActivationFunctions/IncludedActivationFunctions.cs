﻿using System;
namespace neat.src.ActivationFunctions
{
    public class tanh : IActivationFunction
    {
        public double function(double value) =>
            Math.Tanh(value);
    }

    public class sin : IActivationFunction
    {
        public double function(double value) =>
            Math.Sin(value);
    }

    public class cos : IActivationFunction
    {
        public double function(double value) =>
            Math.Cos(value);
    }

    public class abs : IActivationFunction
    {
        public double function(double value) =>
            Math.Abs(value);
    }

    public class clamped : IActivationFunction
    {
        public double function(double value)
        {
            if (value < -1)
                return -1;

            if (value > 1)
                return 1;

            return value;
        }
    }

    public class cubed : IActivationFunction
    {
        public double function(double value) =>
            (value * value * value);
    }

    public class squared : IActivationFunction
    {
        public double function(double value) =>
            (value * value);
    }

    public class sigmoid : IActivationFunction
    {
        public double function(double value) =>
            Math.Tanh(value) * 0.5 + 0.5;
    }
}
