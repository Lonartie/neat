﻿using System;
using NUnit.Framework;


public class ITestFixture
{
    public void Result(bool value, string output)
    {
        Console.WriteLine($"Output: {output}");
        Assert.That(value);
    }

    public void Result<T>(T value1, T value2)
    {
        var mes = "";
        if (!value1.Equals(value2))
            mes = $"value should be {value2.ToString()} but is {value1.ToString()}\nFAILED!";
        else
            mes = $"value is {value1.ToString()} as it should be: {value2.ToString()}\nPASSED!";
        Console.WriteLine(mes);
        Assert.That(value1.Equals(value2));
    }

    public void Result(params (object, object)[] objects)
    {
        var equal = true;
        var ind = -1;
        (object, object) data = (null, null);
        foreach (var obj in objects)
        {
            ind++;
            if (!obj.Item1.Equals(obj.Item2))
            {
                equal = false;
                data = (obj.Item1, obj.Item2);
                break;
            }
        }

        var mes = "";
        if (!equal)
            mes = $"value at index {ind} should be {data.Item2.ToString()} but is {data.Item1.ToString()}\nFAILED!";
        else
            mes = $"all values are equal!\nPASSED!";
        Console.WriteLine(mes);
        Assert.That(equal);
    }
}
